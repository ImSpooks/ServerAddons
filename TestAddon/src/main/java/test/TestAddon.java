package test;

import nl.imspooks.addons.AddOn;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
public class TestAddon extends AddOn {


    @Override
    public void onLoad() {
        getLogger().info("Test Addon is loading (TestAddon#onLoad)");
    }

    @Override
    public void onEnable() {
        getLogger().info("Test Addon is enabled (TestAddon#onEnable)");
    }

    @Override
    public void onDisable() {
        getLogger().info("Test Addon is disabling (TestAddon#onDisable)");
    }
}