package nl.imspooks.addons.bungee;

import lombok.Getter;
import net.md_5.bungee.api.plugin.Plugin;
import nl.imspooks.addons.AddOnManager;
import nl.imspooks.addons.bungee.commands.AddOnCommand;

import java.io.IOException;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
public class AddOnsBungee extends Plugin {

    @Getter private static AddOnsBungee instance;

    @Getter private AddOnManager addOnManager;

    @Override
    public void onEnable() {
        instance = this;

        this.addOnManager = new AddOnManager(this.getLogger(), this.getDataFolder().toPath().resolve("addons"));

        getLogger().info("Loading addons...");
        try {
            this.addOnManager.loadAddons();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.getProxy().getPluginManager().registerCommand(this, new AddOnCommand(this));
    }

    @Override
    public void onDisable() {
        this.addOnManager.shutdown();

        instance = null;
    }
}