package nl.imspooks.addons.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;
import nl.imspooks.addons.AddOn;
import nl.imspooks.addons.Library;
import nl.imspooks.addons.bungee.AddOnsBungee;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
public class AddOnCommand extends Command {

    private AddOnsBungee main;

    public AddOnCommand(AddOnsBungee main, String... aliases) {
        super("addon", "addon.command", aliases);
        this.main = main;
    }

    // shit code but who cares

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (args.length == 0) {
            commandSender.sendMessage(new TextComponent("Use \"addon help\" for the available commands"));
            return;
        }

        switch (args[0].toLowerCase()) {
            default: break;

            case "help":
                commandSender.sendMessage(new TextComponent("AddonLoader commands:"));
                commandSender.sendMessage(new TextComponent("- /addon enable <addon jarfile name>: Enables the target addon"));
                commandSender.sendMessage(new TextComponent("- /addon disable <addon name>: Disables the target addon"));
                commandSender.sendMessage(new TextComponent("- /addon reload <addon name>: Reloads the target addon"));
                commandSender.sendMessage(new TextComponent("- /addon list: Get a list of enabled addons"));
                break;

            case "enable":
            case "disable":
            case "reload":
                StringBuilder target = new StringBuilder();
                for (int i = 1; i < args.length; i++) {
                    target.append(args[i]);
                    if (i < args.length - 1)
                        target.append(" ");
                }

                if (args[0].equalsIgnoreCase("enable")) {
                    try {
                        Files.list(this.main.getAddOnManager().getAddOnPath()).forEach(path -> {
                            if (Files.isDirectory(path))
                                return;

                            if (target.toString().equalsIgnoreCase(path.getFileName().toString())) {
                                try {
                                    this.main.getAddOnManager().loadAddon(path);
                                } catch (IOException | IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
                                    e.printStackTrace();
                                    commandSender.sendMessage(new TextComponent(e.getClass().getSimpleName() + ": " + e.getMessage()));
                                }
                            }
                        });
                        commandSender.sendMessage(new TextComponent("AddOn " + target.toString() + " not found"));
                    } catch (IOException e) {
                        e.printStackTrace();
                        commandSender.sendMessage(new TextComponent(e.getClass().getSimpleName() + ": " + e.getMessage()));
                    }
                } else {
                    for (AddOn addOn : this.main.getAddOnManager().getAddons()) {
                        if (addOn.getAddOnProperties().getName().equalsIgnoreCase(target.toString())) {
                            try {
                                if (args[0].equalsIgnoreCase("reload")) {
                                    commandSender.sendMessage(new TextComponent("Reloading " + addOn.getAddOnProperties().getName()));
                                    this.main.getAddOnManager().reloadAddon(addOn);
                                    commandSender.sendMessage(new TextComponent("Reloaded " + addOn.getAddOnProperties().getName()));
                                } else {
                                    commandSender.sendMessage(new TextComponent("Disabling " + addOn.getAddOnProperties().getName() + "..."));
                                    this.main.getAddOnManager().disableAddon(addOn);
                                    commandSender.sendMessage(new TextComponent(addOn.getAddOnProperties().getName() + " is now disabled"));
                                }
                                return;
                            } catch (IOException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
                                e.printStackTrace();
                                commandSender.sendMessage(new TextComponent(e.getClass().getSimpleName() + ": " + e.getMessage()));
                            }
                        }
                    }
                    if (args[0].equalsIgnoreCase("reload")) {
                        commandSender.sendMessage(new TextComponent("AddOn " + target.toString() + " not found"));
                        return;
                    }

                    for (Library addOn : this.main.getAddOnManager().getLibraries()) {
                        if (addOn.getName().equalsIgnoreCase(target.toString())) {
                            try {
                                commandSender.sendMessage(new TextComponent("Disabling " + addOn.getName() + "..."));
                                this.main.getAddOnManager().disableLibrary(addOn);
                                commandSender.sendMessage(new TextComponent(addOn.getName() + " is now disabled"));
                                return;
                            } catch (IOException e) {
                                e.printStackTrace();
                                commandSender.sendMessage(new TextComponent(e.getClass().getSimpleName() + ": " + e.getMessage()));
                            }
                        }
                    }
                    commandSender.sendMessage(new TextComponent("AddOn " + target.toString() + " not found"));
                }

                break;

            case "list":
                List<String> addons = new ArrayList<>();
                this.main.getAddOnManager().getAddons().forEach(addon -> addons.add(addon.getAddOnProperties().getName()));
                commandSender.sendMessage(new TextComponent("AddOns: " + Arrays.toString(addons.toArray(new String[0]))));

                List<String> libraries = new ArrayList<>();
                this.main.getAddOnManager().getLibraries().forEach(library -> libraries.add(library.getName()));
                if (!libraries.isEmpty()) {
                    commandSender.sendMessage(new TextComponent("Libraries: " + Arrays.toString(libraries.toArray(new String[0]))));
                }
                break;
        }
    }
}