package nl.imspooks.addons;

import lombok.NonNull;
import nl.imspooks.addons.logger.AddOnLogger;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
public class AddOnManager {

    private final Logger logger;
    private final Path addOnPath;

    private final List<Library> libraries = Collections.synchronizedList(new ArrayList<>());
    private final List<AddOn> addons = Collections.synchronizedList(new ArrayList<>());

    /**
     * @param logger Logger instance
     * @param addOnPath Path for the addons
     */
    public AddOnManager(Logger logger, Path addOnPath) {
        this.logger = logger;
        this.addOnPath = addOnPath;
    }

    /**
     * Load all addons in the specified addon folder
     *
     * @throws IOException If the loading of the addons fails for any reason
     */
    public void loadAddons() throws IOException {
        if (!Files.exists(this.addOnPath)) {
            Files.createDirectories(this.addOnPath);
        }


        List<Path> libraries = new ArrayList<>();
        List<Path> addons = new ArrayList<>();
        Files.list(this.addOnPath).filter(path -> path.getFileName().toString().toLowerCase().endsWith(".jar")).forEach(path -> {
            if (this.hasProperties(path)) {
                addons.add(path);
            } else {
                libraries.add(path);
            }
        });

        // Load libraries before addons
        libraries.forEach(path -> {
            try {
                this.loadAddon(path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // Load add ons
        addons.forEach(path -> {
            try {
                this.loadAddon(path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     *
     * @param path Target path
     * @return AddOn properties
     * @throws IOException When failed to load
     */
    private Properties getAddOnProperties(@NonNull Path path) throws IOException {
        JarFile jar = new JarFile(path.toFile());

        JarEntry desc = jar.getJarEntry("addon.properties");
        if (desc == null) {
            return null;
        }
        final Properties properties = new Properties();
        try (final InputStream stream = jar.getInputStream(desc)) {
            properties.load(stream);
        }
        return properties;
    }

    /**
     * @return Active libraries
     */
    public List<Library> getLibraries() {
        return libraries;
    }

    /**
     * @return Active addons
     */
    public List<AddOn> getAddons() {
        return addons;
    }


    /**
     * Load an addon
     *
     * @param path AddOn path
     * @throws IOException When an IO error occurs
     * @throws InstantiationException When the creation of a class using reflection fails
     * @throws IllegalAccessException When the reflection code fails
     */
    public void loadAddon(@NonNull Path path) throws IOException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Properties properties = this.getAddOnProperties(path);

        // No properties found, must be a library
        if (properties == null) {
            Library library = new AddOnClassLoader(this.getClass().getClassLoader(), path).getLibrary();
            this.logger.info(String.format("Added library %s", library.getName()));
            this.libraries.add(library);
            return;
        }

        // Get addon properties
        AddOnProperties addOnProperties = AddOnProperties.fromProperties(properties);

        // Create addon instance
        AddOn addOn = new AddOnClassLoader(this.getClass().getClassLoader(), path, addOnProperties).getAddOn();
        // Set the data path for addon
        addOn.setDataPath(this.addOnPath.resolve(addOnProperties.getName().replaceAll("@\"[\\\\/:*?\"<>|]", "")));

        // Create and set logger instance
        Logger logger = new AddOnLogger(addOn, this.logger);
        logger.setParent(this.logger);
        addOn.setLogger(logger);

        // Enable the addon
        addOn.enable();
        this.addons.add(addOn);
    }

    /**
     * Disables and removes the addon
     *
     * @param addOn Addon instance
     * @throws IOException When removing the classloader fails
     */
    public void disableAddon(AddOn addOn) throws IOException {
        addOn.disable();
        addOn.getClassLoader().close();
        this.addons.remove(addOn);
    }
    /**
     * Removes the library
     *
     * @param library Library instance
     * @throws IOException When removing the classloader fails
     */
    public void disableLibrary(Library library) throws IOException {
        library.getClassLoader().close();
        this.libraries.remove(library);
    }

    public void reloadAddon(AddOn addOn) throws IOException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Path path = addOn.getClassLoader().getPath();
        this.disableAddon(addOn);
        this.loadAddon(path);
    }

    /**
     * Called when the java instance is shutting down
     */
    public void shutdown() {
        while (!this.addons.isEmpty()) {
            try {
                this.disableAddon(this.addons.get(0));
            } catch (IOException e) {
                e.printStackTrace();
                this.addons.remove(0);
            }
        }

        while (!this.libraries.isEmpty()) {
            try {
                this.disableLibrary(this.libraries.get(0));
            } catch (IOException e) {
                e.printStackTrace();
                this.libraries.remove(0);
            }
        }
    }

    /**
     * @param path Target path
     * @return {@code true} when the target has properties, {@code false} otherwise
     */
    private boolean hasProperties(Path path) {
        try {
            return this.getAddOnProperties(path) != null;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * @return Path of where the addons are stored
     */
    public Path getAddOnPath() {
        return addOnPath;
    }
}