package nl.imspooks.addons.logger;

import nl.imspooks.addons.AddOn;

import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
public class AddOnLogger extends Logger {
    private final String pluginName;

    public AddOnLogger(AddOn plugin, Logger parent) {
        super(plugin.getClass().getCanonicalName(), null);

        this.pluginName = "[" + plugin.getAddOnProperties().getName() + "] ";
        this.setParent(parent);
    }

    public void log(LogRecord logRecord) {
        logRecord.setMessage(this.pluginName + logRecord.getMessage());
        super.log(logRecord);
    }
}