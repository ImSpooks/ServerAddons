package nl.imspooks.addons;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Properties;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
@RequiredArgsConstructor
@Getter
public class AddOnProperties {
    private final String name;
    private final String version;
    private final String main;

    static AddOnProperties fromProperties(Properties properties) {
        if (!properties.containsKey("name") || !properties.containsKey("version") || !properties.containsKey("main"))
            throw new AssertionError("addon.properties does not have name, version and/or main. ");

        return new AddOnProperties(properties.getProperty("name"), properties.getProperty("version"), properties.getProperty("main"));
    }
}