package nl.imspooks.addons;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.nio.file.Path;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
@EqualsAndHashCode
@ToString
public abstract class AddOn {

    private boolean enabled;
    private AddOnClassLoader classLoader;
    private AddOnProperties addOnProperties;
    private Logger logger;
    private Path dataPath;

    private final UUID uuid = UUID.randomUUID();

    /**
     * Enables the addon
     */
    void enable() {
        if (this.enabled)
            throw new IllegalStateException("AddOn is already enabled.");
        this.logger.getParent().info(String.format("Enabling %s %s", this.getAddOnProperties().getName(), this.getAddOnProperties().getVersion()));
        this.onLoad();
        this.onEnable();
        this.enabled = true;
    }

    /**
     * Disables the addon
     */
    void disable() {
        if (!this.enabled)
            throw new IllegalStateException("AddOn is already disabled.");
        this.logger.getParent().info(String.format("Disabling %s %s", this.getAddOnProperties().getName(), this.getAddOnProperties().getVersion()));
        this.onDisable();
        this.enabled = false;
    }

    /**
     * Called when the addon enables
     */
    public abstract void onEnable();

    /**
     * Called when the addon disabled
     */
    public abstract void onDisable();

    /**
     * Called when the addon is loading, before when its fully enabled
     */
    public abstract void onLoad();

    /**
     * @return {@code true} when the addon is enabled, {@code false} otherwise
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Set the addon status to either enabled or disabled
     *
     * @param enabled Status
     */
    void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return Class Loader instance
     */
    public AddOnClassLoader getClassLoader() {
        return classLoader;
    }

    /**
     * Set the class loader instance
     *
     * @param classLoader ClassLoader
     */
    void setClassLoader(AddOnClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    /**
     * @return AddOn Properties
     */
    public AddOnProperties getAddOnProperties() {
        return addOnProperties;
    }

    /**
     * Set the addon properties
     *
     * @param addOnProperties AddOn Properties
     */
    void setAddOnProperties(AddOnProperties addOnProperties) {
        this.addOnProperties = addOnProperties;
    }

    /**
     * @return Logger instance
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Set the logger instance
     *
     * @param logger Logger instance
     */
    void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * @return Data path
     */
    public Path getDataPath() {
        return dataPath;
    }

    /**
     * Set the data path
     *
     * @param dataPath Data path
     */
    void setDataPath(Path dataPath) {
        this.dataPath = dataPath;
    }
}