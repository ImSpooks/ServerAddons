package nl.imspooks.addons;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
@RequiredArgsConstructor
public
class Library {
    @Getter
    private final String name;
    @Getter(AccessLevel.PACKAGE)
    private final AddOnClassLoader classLoader;
}