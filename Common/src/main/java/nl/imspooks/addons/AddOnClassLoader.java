package nl.imspooks.addons;

import lombok.NonNull;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;

/**
 * Created by Nick on 22 Aug 2020.
 * Copyright © ImSpooks
 */
public class AddOnClassLoader extends URLClassLoader {

    private final Path path;

    private Library library;
    private AddOn addOn;

    AddOnClassLoader(@NonNull ClassLoader classLoader, @NonNull Path path) throws IOException {
        super(new URL[] {path.toUri().toURL()}, classLoader);

        this.path = path;

        String fileName = this.path.getFileName().toString();
        this.library = new Library(fileName.substring(0, fileName.length() - 4), this);
    }

    AddOnClassLoader(@NonNull ClassLoader classLoader, @NonNull Path path, @NonNull AddOnProperties properties) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        super(new URL[]{path.toUri().toURL()}, classLoader);
        this.path = path;

        Class<?> mainClass;
        try {
            mainClass = Class.forName(properties.getMain(), true, this);
        } catch (final ClassNotFoundException e) {
            throw new IOException("Could not find main class for addOn " + properties.getName());
        }

        Class<? extends AddOn> addOnMain = mainClass.asSubclass(AddOn.class);

        this.addOn = addOnMain.getConstructor().newInstance();
        this.addOn.setClassLoader(this);
        this.addOn.setAddOnProperties(properties);
    }

    /**
     * @return Path of the addon
     */
    public Path getPath() {
        return path;
    }

    /**
     * @return Library instance
     */
    public Library getLibrary() {
        return library;
    }

    /**
     * @return AddOn instance
     */
    public AddOn getAddOn() {
        return addOn;
    }
}